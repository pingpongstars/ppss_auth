from invoke import task


def project_check(project):
    if not project:
        print("Please provide a project name -p")
        return False

def langs_check(langs):
    if not langs:
        return ['it','en']
    else:
        return langs
    print("Please provide at least one lang -l (es. -l it -l en)")
    return False

@task
def extract(c, project="ppss_auth"):
    """extract strings from py, jinja2, mako"""
    project_check(project)
    c.run(f"pybabel extract -F babel.ini -o {project}/locale/{project}.pot {project}")
    c.run(f"pybabel extract -F babel.ini -k _t:2 -o {project}/locale/{project}.pot {project}") 

@task(extract, iterable=['langs'])
def update(c, project="ppss_auth", langs=['it','en']):
    """Update message strings"""
    project_check(project)
    langs = langs_check(langs)
    for lang in langs:
        c.run(f"msgmerge --update {project}/locale/{lang}/LC_MESSAGES/{project}.po {project}/locale/{project}.pot")

@task(iterable=['langs'])
def compile(c, project="ppss_auth", langs=[]):
    """Update message strings"""
    project_check(project)
    langs = langs_check(langs)
    for lang in langs:
        print(f"compilo {lang}")
        c.run(f"msgfmt -o {project}/locale/{lang}/LC_MESSAGES/{project}.mo {project}/locale/{lang}/LC_MESSAGES/{project}.po")

@task(iterable=['langs'])
def setup(c, project="ppss_auth", langs=[]):
    """Setup localization folder"""
    project_check(project)
    langs_check(langs)
    for lang in langs:
        c.run(f"mkdir -p {project}/locale/{lang}/LC_MESSAGES")
        c.run(f"msginit -l {lang} -o {project}/locale/{lang}/LC_MESSAGES/{project}.po --input {project}/locale/{project}.pot")