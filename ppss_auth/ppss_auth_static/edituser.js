function sendForm(event) {
  const form = event.target;
  const url = new URL(form.action || window.location.href);
  const formData = new FormData(form, event.submitter);
  const searchParameters = new URLSearchParams(formData);

  const options = {
    method: form.method,
    headers: new Headers({
      Accept: "application/json",
      "X-CSRF-Token": document.querySelector('input[name="X-CSRF-Token"]')
        .value,
    }),
  };

  if (options.method === "post") {
    // Modify request body to include form data
    options.body =
      form.enctype === "multipart/form-data" ? formData : searchParameters;
  } else {
    // Modify URL to include form data
    url.search = searchParameters;
  }

  fetch(url, options)
    .then((response) => response.json())
    .then((json) => {
      console.log(json);
      const alert = form.querySelector(".alert");
      if (json.res) {
        alert.classList.add("alert-success");
      } else {
        alert.classList.add("alert-danger");
        form.reset();
      }
      alert.textContent = json.msg;
      alert.hidden = false;
    })
    .catch( (error) => {
      console.warn(error);
    });
  event.preventDefault();
}

document.addEventListener("submit", (event) => {
  if (event.target.matches("form[data-ajax-form]")) {
    sendForm(event);
  }
});
