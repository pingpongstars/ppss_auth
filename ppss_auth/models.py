import sys

PY2 = sys.version_info[0] == 2
if not PY2:
    unicode = str

from datetime import datetime, timedelta

from sqlalchemy import (
    Table,
    Column,
    Index,
    Integer,
    Text,
    Unicode,UnicodeText,
    DateTime,
    ForeignKey,
    desc, asc,UniqueConstraint
)
from sqlalchemy.orm import relationship, backref

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.schema import MetaData
NAMING_CONVENTION = {
    "ix": 'ix_%(column_0_label)s',
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
}

import logging

from sqlalchemy.orm import joinedload
from pyramid.request import Request

metadata = MetaData(naming_convention=NAMING_CONVENTION)
Base = declarative_base(metadata=metadata)

from .constants import Conf
from .ppss_auth_utils.password import getPasswordDigest


l = logging.getLogger('ppssauth.models')


ppssuserlkppssgroup = Table('ppssuser_lk_ppssgroup', Base.metadata,
    Column('user_id',Integer,ForeignKey('ppss_user.id', ondelete="CASCADE"), primary_key=True),
    Column('group_id',Integer,ForeignKey('ppss_group.id', ondelete="CASCADE"), primary_key=True )
)
ppssgrouplkppsspermission = Table('ppssgroup_lk_ppsspermission', Base.metadata,
    Column('group_id',Integer,ForeignKey('ppss_group.id', ondelete="CASCADE"), primary_key=True),
    Column('permission_id',Integer,ForeignKey('ppss_permission.id', ondelete="CASCADE"), primary_key=True )
)


class commonTable():
    @classmethod
    def byId(cls,id,dbsession):
        return dbsession.query(cls).filter(cls.id == id).first()

    # @classmethod
    # def all(cls,dbsession):
    #     return dbsession.query(cls).all()
    
    @classmethod
    def all(cls,DBSession,orderby=None,orderdir='asc'):
        q = DBSession.query(cls)
        if orderby is not None:
            if orderdir == 'asc':
                q = q.order_by(getattr(cls,orderby).asc() )
            else:
                q = q.order_by((getattr(cls,orderby).desc()  ))
        return q.all()
        #return cls.orderAll(DBSession.query(cls)).all()

    @classmethod
    def byField(cls,field,value,DBSession,orderby=None,orderdir='asc'):
        q = DBSession.query(cls).filter(getattr(cls, field)==value)
        if orderby is not None:
            if orderdir == 'asc':
                q = q.order_by(getattr(cls,orderby).asc() )
            else:
                q = q.order_by((getattr(cls,orderby).desc()  ))
        return q.all()
    
    @classmethod
    def byFields(cls,fields,DBSession,orderby=None,orderdir='asc'):
        q = DBSession.query(cls)
        for i,field in enumerate(fields):
            q = q.filter(getattr(cls, field[0])==field[1])
        if orderby is not None:
            if orderdir == 'asc':
                q = q.order_by(getattr(cls,orderby).asc() )
            else:
                q = q.order_by((getattr(cls,orderby).desc()  ))
        return q.all()
    
    @classmethod
    def firstByField(cls,field,value,DBSession):
        return DBSession.query(cls).filter(getattr(cls, field)==value).first()

class LOGINREASON():
    OTPSKIPPED = (3,"OTP not required")
    OTPPASSED = (2,"OTP Ok")
    OK = (1,"Ok")
    VALIDATIONERROR = (0,"invalid password")
    BLOCKED = (-1,"too many recent fails")
    DISABLED = (-2,"user disabled")
    DONTEXIST = (-3,"user does not exist")
    OTPERROR = (-4,"OTP error")

class LoginError():
    def __init__(self,validlogin,enabled,failcount):
        self.validlogin = validlogin
        self.enabled = enabled
        self.failcount = failcount

    def __bool__(self):
        # return self.validlogin and self.enabled and self.failcount
        return self.result() == 1
    
    def __result(self):
        if self.enabled is None:
            return LOGINREASON.DONTEXIST
        if not self.enabled:
            return LOGINREASON.DISABLED
        if self.failcount:
            return LOGINREASON.BLOCKED
        if not self.validlogin:
            return LOGINREASON.VALIDATIONERROR
        return LOGINREASON.OK


    def result(self):
        return self.__result()[0]
        #return 1 if self.failcount and self.validlogin and self.enabled else 0 if self.validlogin else -1 if self.failcount else -2
                
    def blocked(self):
        return self.failcount is not None and not self.failcount

    def resultreason(self):
        return self.__result()[1]
        # if self.enabled is None:
        #     return "user does not exist"
        # if not self.enabled:
        #     return "user disabled"
        # if not self.validlogin:
        #     return "invalid password"
        # if not self.failcount:
        #     return "too many recent fails"
        # return "Ok"

    def __str__(self):
        return self.username

class PPSsuser(Base,commonTable):
    __tablename__   = 'ppss_user'
    id              = Column(Integer, primary_key=True)
    username        = Column(Unicode(128),unique=True)

    ###EMAIL
    email           = Column(Unicode(128),unique=True)

    password        = Column(Unicode(1024))
    insertdt        = Column(DateTime,default=datetime.now)
    updatedt        = Column(DateTime,default=datetime.now,onupdate=datetime.now)
    lastlogin       = Column(DateTime)
    enabled         = Column(Integer,default=1) # 0 disabled, 1 active, -1 need activation
    magicnumber     = Column(Text())   #Conf.saltforhash
    createdby       = Column(Integer)
    disabledby      = Column(Integer)
    passwordexpire  = Column(DateTime, default = None)

    groups = relationship("PPSsgroup",secondary=ppssuserlkppssgroup,lazy='joined',cascade="all", 
        backref=backref('users',lazy='select',order_by='PPSsuser.username'))

    tokens = relationship("PPSsResetTokens", back_populates='user')

    otp_hash = Column(Unicode(64),unique=True)

    phone_prefix = Column(Unicode(8))
    phone_number = Column(Unicode(16), unique=True)

    superusercache = None
    permissionscache = None
    permissionsmapcache = None
    groupmapcache = None

    @classmethod
    def checkLogin(cls,user,password,dbsession, ipaddr = None):
        return PPSsuser.checkCryptedLogin(user,getPasswordDigest(password),dbsession,ipaddr=ipaddr)

    @classmethod
    def checkCryptedLogin(cls,user,password,dbsession,ipaddr = None):
        # .filter(cls.password==password).filter(cls.enabled==1)
        res = dbsession.query(cls).filter(cls.username==user) \
            .options(joinedload('groups')) \
            .options(joinedload('groups.permissions')) \
            .all()
        if len(res)==1:
            res:PPSsuser = res[0]
            failcountquery = (
                dbsession.query(PPSsloginhistory)
                .filter(PPSsloginhistory.user_id == res.id)
                .filter(PPSsloginhistory.result == LOGINREASON.VALIDATIONERROR[0])
                .filter(
                    PPSsloginhistory.insertdt
                    > datetime.now() - timedelta(seconds=Conf.loginfailinterval)
                )
            )
            if ipaddr:
                failcountquery = failcountquery.filter(PPSsloginhistory.ipaddress == ipaddr)
            failcount = failcountquery.count()            
            valid = LoginError(
                res.password == password,
                res.enabled == 1,
                failcount > Conf.loginfailthreshold,
            )
            # res.password == password and res.enabled == 1
        else:
            res = None
            valid = LoginError(None,None,None)
        return res,valid
        # return res[0] if len(res)==1 else None

    def passwordExpired(self,now = None):
        if now is None:
            now = datetime.now()
        l.debug("-------- user:{},now:{},expire:{},res:{}".format(self.username,now,self.passwordexpire,
                not ((self.passwordexpire is None) or self.passwordexpire>now)))

        notexpired = ((self.passwordexpire is None) or (self.passwordexpire>now))
        return not notexpired

    def is2faEnabled(self):
        return bool(self.otp_hash)

    __principals = None
    def getPrincipals(self):
        if self.__principals is None:
            principals = [str("g:"+group.name ) for group in self.groups  ] 
            if self.isSuperUser():
                principals += ["g:admin","g:sysadmin"]
            l.debug("++++ principals:{}".format(principals))
            self.__principals = principals

        return self.__principals,self.isSuperUser()

    def todict(self):
        return { "id": self.id, "username":self.username,"enabled":self.enabled}

    def setPassword(self,password,canexpire = True):
        dig = getPasswordDigest(password)
        self.password= dig
        if Conf.passwordexpire and canexpire:
            self.passwordexpire = datetime.now() + timedelta(days = Conf.passwordexpire)
        else:
            self.passwordexpire = None        
        self.passowrdhistory.append( PPSspasswordhistory(password = dig)  )
        return self

    def getPermissions(self):
        if self.permissionscache is None:
            result = set()
            for g in self.groups:
                if g.enabled:
                    for p in g.permissions:
                        result.add( (p.id, p.name, p.permtype) )
            self.permissionscache = list(set(result))
        return self.permissionscache
        # return set([p.name for p in [g.permissions for g in self.groups if g.enabled]] )

    def getPermissionsMap(self):
        if self.permissionsmapcache is None:
            permlist = self.getPermissions()
            self.permissionsmapcache = set([p[1] for p in permlist])
        return self.permissionsmapcache

    def isSuperUser(self):
        if self.superusercache is None:
            self.superusercache = False
            l.info("checking if {} is superuser against this name:'{}' and permissions:{}".format(self.username,Conf.adminname,self.getPermissionsMap()  ))
            if self.username == Conf.adminname:
                self.superusercache = True
            elif 'sysadmin' in self.getPermissionsMap():
                self.superusercache = True
            else:
                l.info("not a super user: {}".format( self.getPermissionsMap() ))
        return self.superusercache

    def hasPermission(self, permission):
        if self.isSuperUser():
            return True
        return permission in self.getPermissionsMap()

    def getGroupsMap(self):
        if self.groupmapcache is None:
            grouplist = []
            for g in self.groups:
                grouplist.append(g.name)
            self.groupmapcache = set(grouplist)
        return self.groupmapcache

    def isInGroup(self,groupname,whitelie=True):
        if self.isSuperUser() and whitelie:return True
        return groupname in self.getGroupsMap()

    def needVerifyOTP(self, request:Request):
        since = datetime.now() - timedelta(hours=Conf.hoursvalid2fa)
        # since = datetime.now() - timedelta(seconds=20)
        success_login = (
            request.dbsession.query(PPSsloginhistory)
            .filter(PPSsloginhistory.user_id == self.id)
            .filter(PPSsloginhistory.ipaddress == request.remote_addr)
            .filter(PPSsloginhistory.result == LOGINREASON.OTPPASSED[0])
            .filter(PPSsloginhistory.insertdt > since)
        ).first()
        if success_login:
            return False
        return True

    def __unicode__(self):
        return u"<PPSsuser ({id}-{name},{enabled})>".format(id=self.id,name=self.username, enabled=self.enabled)

    def __str__(self): 
        return self.__unicode__()

class PPSsResetTokens(Base, commonTable):
    __tablename__   = 'ppss_resettokens'
    id              = Column(Integer, primary_key=True)
    user_id         = Column(Integer, ForeignKey('ppss_user.id'))
    token           = Column(Unicode(256), unique=True)
    token_expiry    = Column(DateTime,default=lambda:datetime.now() + timedelta(minutes = 10))
    reset_type      = Column(Unicode(16))
    additionaldata  = Column(Text())

    user = relationship("PPSsuser", back_populates='tokens')

class PPSspasswordhistory(Base):
    __tablename__   = 'ppss_passwordhistory'
    id              = Column(Integer, primary_key=True)
    user_id         = Column(Integer, ForeignKey('ppss_user.id'))
    insertdt        = Column(DateTime,default=datetime.now)
    password        = Column(Unicode(1024))

    user = relationship("PPSsuser", backref=backref('passowrdhistory',order_by="desc(PPSspasswordhistory.id)" ))

class PPSsloginhistory(Base):
    __tablename__   = 'ppss_loginhistory'
    id              = Column(Integer, primary_key=True)
    user_id         = Column(Integer, ForeignKey('ppss_user.id'))
    ipaddress       = Column(Unicode(128))
    insertdt        = Column(DateTime,default=datetime.now)
    result          = Column(Integer)
    resultreason    = Column(Unicode(128))
    username        = Column(Unicode(128))

# result int
# -3 = invalid otp
# -2 = ??
# -1 = ?? blocked ?
# 0 = failed
# 1 = success
# 2 = logged in with otp
# 3 = logged in without otp


class PPSsgroup(Base,commonTable):
    __tablename__   = 'ppss_group'
    id     = Column(Integer, primary_key=True)
    name   = Column(Unicode(128),unique=True)
    enabled= Column(Integer,default=1)
    permissions = relationship("PPSspermission",cascade="all",secondary=ppssgrouplkppsspermission  ,backref=backref('groups'))

    permissionsmapcache = None
    permissionscache = None

    def __unicode__(self):
        return u"<PPSsgroup {name} ({id})>".format(name=self.name,id=self.id)
    def __str__(self):
        return self.__unicode__()

    def todict(self):
        return {'id':self.id,"name":self.name}

    def userdict(self):
        return [x.todict() for x in self.users]

    def permdict(self):
        return [x.todict() for x in self.permissions]

    def getPermissionsMap(self):
        if self.permissionsmapcache is None:
            self.permissionscache = []
            for p in self.permissions:
                self.permissionscache.append(p.name)
            self.permissionsmapcache = set(self.permissionscache)
        return self.permissionsmapcache

    def hasPermission(self,permissionname):
        return permissionname in self.getPermissionsMap()

    @classmethod
    def byName(cls,name,dbsession):
        return dbsession.query(cls).filter(cls.name==name).first()

class PPSspermission(Base,commonTable):
    __tablename__   = 'ppss_permission'
    id     = Column(Integer, primary_key=True)
    name   = Column(Unicode(128),unique=True)
    permtype   = Column(Integer,default=0)  #1 is for built-in permissions
    systemperm = Column(Unicode(4),default=u'y')

    def __unicode__(self):
        return u"<PPSspermission {name} ({id})>".format(name=self.name,id=self.id)
    def __str__(self):
        return self.__unicode__()

    def todict(self):
        return {'id':self.id,"name":self.name,"permtype":self.permtype}


import pkg_resources  # part of setuptools

class DBVersion(Base):
    __tablename__   = 'module_db_version'
    modulename = Column(Unicode(128),primary_key=True)
    moduleversion = Column(Unicode(64),primary_key=True)
    dbversion = Column(Unicode(64))
    insertdt  = Column(DateTime,default=datetime.now,onupdate=datetime.now)

    myname = "ppss_auth"

    @staticmethod
    def updateDB(session):
        version = session.query(DBVersion).filter(DBVersion.modulename == DBVersion.myname).first()
        moduldeversion = pkg_resources.require("ppss_auth")[0].version
        l.debug("read version in db {} module version {}".format(version,moduldeversion) )
        if version is None:
            session.add(DBVersion(modulename=DBVersion.myname,moduleversion=moduldeversion,dbversion="1.0") ) 
        else:
            version.moduleversion = moduldeversion
            version.dbversion = "1.0"
