��    ,      |  ;   �      �     �  	   �     �     �     �  #   �          +     9     A  
   I     T     [     ^     j     p     u     x     �     �     �     �     �     �     �     �     �     �            1   $     V     k     |  
   �     �     �     �     �     �  D   �            c       |     �     �     �     �  *   �     �       
     	   %     /     ?     F     K     Z     `     e     h          �     �     �  
   �     �     �     �      	     	      	     $	  6   5	     l	     �	     �	     �	     �	     �	     �	     �	     �	  B   �	     '
     0
                  "                                  +   (              *      
   #   	                          %            &       !       '   )                                $                        ,    Action Add Group Add Perm Add User Apply Change password for user {username} Edit user {username} Enable group: Enable: Enabled Group name Groups Hi Last access Login Name No Not registered yet? Perm to add Perm to remove Permissions Please sign in Register Register now System User to add User to remove Username Yes You are already logged in as You can proceed to your main page following this  confirm new password current password delete group name link modify new password permissionname remove group something went wrong with your login. Please check your informations update username Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
PO-Revision-Date: 2020-05-14 19:04+0200
Last-Translator:  <dan@gorgonzurla.localdomain>
Language-Team: Italian
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.8.0
Plural-Forms: nplurals=2; plural=(n != 1);
 Azione Aggiungi gruppo Aggiungi permesso Aggiungi utente Applica Campia la password per l'utente {username} Modifica dell'utente {username} Abilita il gruppo Abilitato: Abilitato Nome del gruppo Gruppi Ciao Ultimo accesso Entra Nome No Non ancora registrato? Permessi da aggiungere Permessi da rimuovere Permessi Esegui l'accesso Registrati Registrati adesso Sistema Utenti da aggiungere Utenti da rimuovere Nome utente Sì Sei loggato come Puoi procedere alla pagina principale seguendo questo  conferma la password password attuale cancella nome del gruppo link modifica nuova password nomepermesso rimuovi gruppo Qualcosa è andato storto. Controlla le tue credenziali e riprova. aggiorna nome utente 