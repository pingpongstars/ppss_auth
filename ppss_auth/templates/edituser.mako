<%inherit file="${context['midtpl']}" />

<%
usergroups = {}
for g in user.groups:
    usergroups[g.id] = g.name
%>
<div class="row">
    <div class="${bc['xs']}12 col-sm-6">
        %if user and user.username:
        <h3>${_("Edit user {username}", domain='ppss_auth').format(username=user.username)}</h3>
        %endif

        %if request.session.peek_flash():
            %for message in request.session.pop_flash():
            <div class="alert alert-${message.get('type')}" role="alert">
                ${ message.msg }
            </div>
            %endfor
        %endif
        <label for="username">${_('username', domain='ppss_auth')}</label>
        <input class="form-control mb-3" type="text" name="username" value="${user.username}" readonly>
        <input name="X-CSRF-Token" value="${get_csrf_token()}" type="hidden">

        <form action="${request.route_url('ppss:user:email:edit')}" method="post" data-ajax-form class="border p-3">
            <input type="hidden" value="${get_csrf_token()}" name="csrf_token">
            <input type="hidden" value="${user.id}" name="userid">
            <label for="email">${_('email', domain='ppss_auth')}</label>
            <input class="form-control" type="email" name="email" placeholder="test@test.com" value="${user.email if (user and user.email) else ''}" required>
            <button class="btn btn-success my-2" type="submit">${_('Apply', domain='ppss_auth')}</button>
            <div hidden class="alert" role="alert">
            </div>
        </form>

        <form action="${request.route_url('ppss:user:password:edit')}" method="post" data-ajax-form class="border p-3">
            <input type="hidden" value="${get_csrf_token()}" name="csrf_token">
            <input type="hidden" value="${user.id}" name="userid">
            %if requireoldpassword: 
            <label for="currentpassword">${_('current password', domain='ppss_auth')}</label>
            <input class="form-control" type="password" name="currentpassword" placeholder="${_('current password', domain='ppss_auth')}" value="">
            %endif
            <label for="password">${_('password', domain='ppss_auth')}</label>
            <input class="form-control" type="password" name="password" placeholder="${_('password', domain='ppss_auth')}" value="">
            <label for="confirmnewpassword">${_('confirm new password', domain='ppss_auth')}</label>
            <input class="form-control" type="password" name="confirmnewpassword" placeholder="${_('confirm new password', domain='ppss_auth')}" value="">
            <button class="btn btn-success my-2" type="submit">${_('Apply', domain='ppss_auth')}</button>
            <div hidden class="alert" role="alert">
            </div>
        </form>

        %if not selfediting:
        <form action="${request.route_url('ppss:user:enable:edit')}" method="post" data-ajax-form class="border p-3">
            <input type="hidden" value="${get_csrf_token()}" name="csrf_token">
            <input type="hidden" value="${user.id}" name="userid">
            <div class="checkbox">
                <label for="enablecheck">
                    <input id="enablecheck" type="checkbox" value="1" ${'checked="checked"' if user and user.enabled else ''} name="enabled"> ${_('Enable:', domain='ppss_auth')}
                </label>
            </div>
            <button class="btn btn-success my-2" type="submit">${_('Apply', domain='ppss_auth')}</button>
            <div hidden class="alert" role="alert">
            </div>
        </form>
        %endif
        %if ppsauthconf.issuer2fa:
        <div class="border p-3">
            %if selfediting and not user.otp_hash:
            <a href="${request.route_url('2fa:enable')}" class="btn btn-primary my-2">${_('Enable 2FA', domain='ppss_auth')}</a>
            %endif
            %if user.otp_hash:
            <div class="form-check">
                <input checked class="form-check-input" type="checkbox" id="defaultCheck2" disabled>
                <label class="form-check-label text-success" for="defaultCheck2">
                    2FA enabled
                </label>
              </div>
              <a href="${request.route_url('ppss:user:editotp')}" class="btn btn-primary my-2">${_('show/change 2FA', domain='ppss_auth')}</a>
            %endif
        </div>
        %endif
        <form action="${request.route_url('ppss:user:groups:edit')}" method="post" data-ajax-form class="border p-3">
            <input type="hidden" value="${get_csrf_token()}" name="csrf_token">
            <input type="hidden" value="${user.id}" name="userid">
            <div>
                <p class="label">${_('Groups', domain='ppss_auth')}</p>
                %for g in allgroups:
                    <label class="checkbox-inline">
                    <input name="allgroups" type="checkbox" value="${g.id}" ${"checked" if g.id in usergroups else ""}> ${g.name}
                </label>
            %endfor
            </div>
            <button class="btn btn-success my-2" type="submit">${_('Apply', domain='ppss_auth')}</button>
            <div hidden class="alert" role="alert">
            </div>
        </form>

        <!-- <form action="${submiturl}" method="POST" autocomplete="off">
            <input type="hidden" value="${get_csrf_token()}" name="csrf_token">
            <input class="form-control" type="text" name="username" placeholder="${_('username', domain='ppss_auth')}" value="${user.username}" readonly>
            </br>
            <fieldset class="mb-3">
            <input class="form-control" type="email" name="email" placeholder="${_('email', domain='ppss_auth')}" value="${user.email if (user and user.email) else ''}" ${ 'required' if email_is_required else '' } >
            </fieldset>
            %if selfediting or ((request.loggeduser.isSuperUser() == False) and user.id and user.id >= 0):
            <input class="form-control" type="password" name="currentpassword" placeholder="${_('current password', domain='ppss_auth')}" value="">
            </br>
            %endif
            <input class="form-control" type="password" name="password" placeholder="${_('password', domain='ppss_auth')}" value="" autocomplete="new-password">
            </br>
            <input class="form-control" type="password" name="confirmnewpassword" placeholder="${_('confirm new password', domain='ppss_auth')}" value="" autocomplete="new-password">
            %if ppsauthconf.issuer2fa and selfediting and not user.otp_hash:
            <a href="${request.route_url('2fa:enable')}" class="btn btn-primary my-2">${_('Enable 2FA', domain='ppss_auth')}</a>
            %endif
            %if user.otp_hash:
            <div class="form-check">
                <input checked class="form-check-input" type="checkbox" id="defaultCheck2" disabled>
                <label class="form-check-label text-success" for="defaultCheck2">
                    2FA enabled
                </label>
              </div>
              <a href="${request.route_url('ppss:user:editotp')}" class="btn btn-primary my-2">${_('show/change 2FA', domain='ppss_auth')}</a>
            %endif
            
            <div class="checkbox">
                <label for="enablecheck">
                    <input id="enablecheck" type="checkbox" value="1" ${'checked="checked"' if user and user.enabled else ''} name="enabled"> ${_('Enable:', domain='ppss_auth')}
                </label>
            </div>
            <p class="label">${_('Groups', domain='ppss_auth')}</p>
            %for g in allgroups:
                <label class="checkbox-inline">
                    <input name="allgroups" type="checkbox" value="${g.id}" ${"checked" if g.id in usergroups else ""}> ${g.name}
                </label>
            %endfor
            </br>
            <input class="btn btn-success" type="submit" name="submit" value="${_('Apply', domain='ppss_auth')}"/>

            <p class="resultmsg">${msg}</p>
        </form> -->
    </div>
</div>

<%block name="ppssauth_footerjs">
<script src="${request.static_url('ppss_auth:ppss_auth_static/edituser.js')}"></script>
</%block>
