<%inherit file="${context['midtpl']}" />


<style>
    .toggled {
        display: none;
    }

    :checked+.toggled {
        display: block;
    }
</style>
<div class="row">
    <div class="${bc['xs']}12 col-sm-6">
        <h3>your 2fa setup</h3>
        %if errmsg:
            <div class="alert alert-danger">
                ${errmsg}
            </div>
            %endif
        <p>This is the QR code you can use to setup 2fa on your new device </p>
        <img class="mb-3" width="200" height="200" src="${img_str}" alt="">
        <p class="small">SECRET KEY: ${current_otp_hash}</p>
        <p>You can also regenerate a new otp setup checking this checkbox</p>
        <div class="form-check">
            <label class="form-check-label" for="defaultCheck1">
                new otp
            </label>
            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
            <div class="toggled">
                <p>After setting up you account confirm inserting in the form below 6 digit provided by your auth app
                </p>
                <img class="mb-3" width="200" height="200" src="${img_str_new}" alt="">
                <p class="small">SECRET KEY: ${otp_hash}</p>
                <form method="post">
                    <input type="hidden" value="${get_csrf_token()}" name="csrf_token">
                    <input type="hidden" name="otp_hash" value="${otp_hash}">
                    <%include file='partials/otp.html' />
                    <button class="btn btn-success" type="submit">Confirm</button>
                </form>
            </div>
        </div>
    </div>
</div>
