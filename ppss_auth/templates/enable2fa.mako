<%inherit file="${context['midtpl']}" />

<div class="container">
    <div class="row text-center">
        <div class="${bc['xs']}12 col-md-8 offset-md-2">
            %if errmsg:
            <div class="alert alert-danger my-2">
                ${errmsg}
            </div>
            %endif
            <h3 class="mt-3 mb-0">Enable 2FA authentication</h3>
            <p class="mb-3">It seems you don't have 2fa enabled, please follow these steps:</p>
            <p>1. Download an authenticator app on your phone, below are listed some for your convenience </p>
            <%include  file='partials/auth-apps.html' />
            <p>2. Add new account and scan the QR code below or manually add the uri</p>
            <img class="img-fluid" width="200" height="200" src="${img_str}" alt="">
            <p class="small mb-3"> SECRET KEY: ${otp_hash}</p>
            <p>3. Enter the 6 digit code provided by your authenticator</p>
            <form method="post">
                <input type="hidden" value="${get_csrf_token()}" name="csrf_token">
                <input type="hidden" name="otp_hash" value="${otp_hash}">
                <%include file='partials/otp.html' />
                <button class="btn btn-success my-3" type="submit">Confirm</button>
            </form>
        </div>
    </div>
</div>