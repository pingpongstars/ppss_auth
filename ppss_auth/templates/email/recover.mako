<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Recover</title>
</head>
<body>
    We received a reset/recover password request from this email address. If it was you, to reset your password follow <a href="${reset_link}">this</a> link or enter: 
    <p>
    ${reset_link}
    </p>
    in your browser's navigation bar.
</body>
</html>