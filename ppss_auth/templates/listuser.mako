<%inherit file="${context['midtpl']}" />
<% passwordexpire = request.ppssauthconf.passwordexpire %>
<div>
	%if request.session.peek_flash():
		%for message in request.session.pop_flash():
		<div class="alert alert-${message.get('type')}" role="alert">
			${ message.get('msg') }
		</div>
		%endfor
    %endif
	<form class="form-inline" method="post">
		<input type="hidden" value="${get_csrf_token()}" name="csrf_token">
		<input class="form-control" type="text" name="username" placeholder="username" id="">
		<button class="btn btn-success" type="submit">${_('Add User', domain='ppss_auth')}</button>
	</form>
</div>
<table class="table">
	<thead>
		<tr>
			<th>${_('Username', domain='ppss_auth')}</th>
			<th>${_('Enabled', domain='ppss_auth')}</th>
			%if passwordexpire:
				<th>${_('Password expires', domain='ppss_auth')}</th>
			%endif
			<th>${_('Last access', domain='ppss_auth')}</th>
			<th>${_('Action', domain='ppss_auth')}</th>
		</tr>
	</thead>
	<tbody>
		%for i,e in enumerate(elements):
			<tr>
				<td>${e.username}</td>
				<td>${_("Yes", domain='ppss_auth') if e.enabled else _("No", domain='ppss_auth')}</td>
				%if passwordexpire:
					<th>${e.passwordexpire.strftime( _('%m/%d/%Y', domain='ppss_auth') ) if e.passwordexpire else " - "}</th>
				%endif
				<td>${e.lastlogin.strftime( _('%Y-%m-%d', domain='ppss_auth') ) if e.lastlogin else " - "}</td>
				<td>
					<a class="btn btn-success" href="${request.route_url('ppss:user:edit',elementid=e.id)}">${_('modify', domain='ppss_auth')}</a><br/>

				 </td>
			</tr>
		%endfor

	</tbody>


</table>

