import base64
from io import BytesIO
import json
import re
import secrets

import pyotp
import qrcode
import requests
from ppss_auth.ppss_auth_utils.emailclient import sendConfirmEmailUseCase, sendPasswordResetEmailUseCase
from ppss_auth.ppss_auth_utils.otp import OtpValidity
from ppss_auth.ppss_auth_utils.password import getPasswordDigest

from pyramid.request import Request
from pyramid.response import Response
from pyramid.authentication import AuthTktCookieHelper
from pyramid.settings import asbool
from pyramid.renderers import render_to_response

from ..constants import Conf
from ..ppss_auth_utils import checkPassword,_,__
from ..models import LOGINREASON, PPSsResetTokens, PPSsuser,PPSsgroup,PPSspermission,PPSsloginhistory
from ..ppss_auth_utils.createdb import constants

from pyramid.view import view_defaults,view_config,forbidden_view_config
from pyramid.httpexceptions import HTTPFound, HTTPNotFound
from beaker.cache import cache_region
from sqlalchemy.exc import InvalidRequestError



import os,datetime,logging
from datetime import timedelta
l = logging.getLogger('ppssauth')

from ..ppss_auth_utils.usersession import UserSession

from pyramid.security import (
    Everyone, Authenticated,
    remember,forget,
    Allow,Deny,
    Everyone,ALL_PERMISSIONS
    )


def getPrincipals(uid,request):
    #groups = request.session.get('principals',[])
    us = request.usersession #UserSession(request.loggeduser,request)
    groups = us.getPrincipals()
    l.debug("####  usergroups:{g}".format(g=groups))
    return groups

#this class build the ACL consumed by ACLRoot
#ACL is in the form: {group name: [list of permission names]} and is derived by ppss_groups and ppss_permission)
#permissions to be used in views are ppss_permission elements.
class ACLBuilder(object):
    def __init__(self,baseACL,dbsession):
        self.baseACL = baseACL
        self.session  = dbsession

    @cache_region('short_term', 'ACLpermissions')
    def buildACL(self):
        l.debug("buildACL cache miss")
        acl = [] 
        try:
            groups = self.session.query(PPSsgroup).filter(PPSsgroup.enabled==1).all()
            for group in groups:
                acl.append( (Allow,
                        str("g:"+group.name),
                        tuple([str(p.name) for p in group.permissions])  
                ) )
        except InvalidRequestError as invalid_session:
            l.exception("**************invalid_session. Something is going very very wrong!!")
            #raise invalid_session
        except Exception as e:
            l.warn("called without a transaction")
        acl = self.baseACL + acl
        l.info("ACLBuilder:acl built: {acl}".format(acl = acl))
        return acl


#This class stores the acl structure used by the root factory
class ACLRoot(object):
    baseACL=[(Allow, Authenticated, 'view'),
        (Allow, 'g:'+constants.SYSADMINGROUP, ALL_PERMISSIONS),
        (Allow, 'ppss_auth:changepassword',"ppss_auth_changepassword"),
        (Allow, 'ppss_auth:otpverify',"ppss_auth_otpverify"),
        (Allow, 'ppss_auth:otpset', "ppss_auth_otpset")
        ]

    lastupdateACL = datetime.datetime.now()
    __acl__ = [
        (Allow, 'g:'+constants.SYSADMINPERM, ALL_PERMISSIONS)
    ]


    def __init__(self, request):
        self.request = request
        aclb = ACLBuilder(ACLRoot.baseACL,request.dbsession)
        ACLRoot.__acl__ = aclb.buildACL()


class ForbiddenRoute():
    def __init__(self,request):
        self.request = request

    def dispatcher(self):
        data, template = self.handler()
        if isinstance(data, dict):
            return render_to_response(template, data, self.request)
        elif isinstance(data, Response):
            return data
        else:
            raise Exception("data must be dict or Response")
        
    def handler(self):
        authcontroller = AuthController(self.request)
        if self.request.loggeduser:
            us = self.request.usersession
            if us.needOTPcheck():
                l.info("********************* OTP Check")
                return authcontroller.verify_otp(),Conf.verify2fatpl
            if us.needOTPSetup():
                l.info("********************* OTP Setup")
                return authcontroller.enable_2fa(),Conf.enable2fatpl
            if us.passwordExpired(): ##  user.passwordExpired():
                l.info("********************* change password")
                return authcontroller.ppsschangepassword(),Conf.changepasswordtemplate
        return authcontroller.login(),Conf.logintemplate

@view_defaults(require_csrf=True)
class AuthController():
    def __init__(self,request:Request):
        request.response.headers["X-Frame-Options"] = "DENY"
        request.response.headers["Content-Security-Policy"] = "frame-ancestors 'none';"
        self.request = request
        self.user = None
        activemenu = ""
        activeaction = ""
        ##request.bt = Conf['bootstrapclasses']

        try:
            mr = self.request.matched_route.name.split(":")
            if len(mr) == 3:
                activemenu = mr[1]
                activeaction = mr[2]
        except:
            pass

        #for all ops on users, get the target user
        self.userid = int(
            self.request.params.get("userid",
                self.request.matchdict.get("elementid",
                    self.request.session[Conf.sessionuser]['id'] if Conf.sessionuser in self.request.session else -1 ) 
                )
            )
        self.user = self.getUserById(self.userid)
        

        self.retdict = {
            'midtpl':Conf.sectiontemplateinherit,
            'supertpl':Conf.mastertemplateinherit,
            'botplinherit':Conf.botemplateinherit,
            'logintpl': Conf.publictemplateinherit,
            'activemenu' : activemenu,
            'activeaction' : activeaction,
            'bc': Conf.bootstrapClasses,
            'ppsauthconf':Conf,
            'msg':"",
            'isCaptchaEnabled': self.isCaptchaEnabled(),
            'email_is_required' : Conf.email_is_required
        }
    
    def getUserById(self,userid):
        if userid<0:
            user = PPSsuser()
        else:
            user = PPSsuser.byId(userid,self.request.dbsession)
        return user

    def isCaptchaEnabled(self) -> bool:
        if Conf.turnstile_sitekey and Conf.turnstile_secretkey:
            return True
        else:
            return False
        
    def verifyCaptcha(self,token) -> bool:
        try:
            r = requests.post('https://challenges.cloudflare.com/turnstile/v0/siteverify', data={
                'secret': Conf.turnstile_secretkey,
                'response': token
            })
            r.raise_for_status()
            return r.json()['success']
        except Exception as e:
            l.exception("Captcha api call failed")
            return False

    def logloginattempt(self,user,validity,username):
        self.request.dbsession.add(
                PPSsloginhistory(
                    user_id = user.id if user else None,
                    ipaddress = self.request.remote_addr,
                    result = validity.result(),
                    resultreason = validity.resultreason(),
                    username = username
                )
            )
        l.info("login attempt for user '{username}' result is '{result}'".format(username=username,result = validity.resultreason()) )
    
    def getPostLoginRoute(self):            
        if 'postloginpage' in self.request.session:
            self.postloginpage = self.request.session['postloginpage']
        else:
            self.postloginpage = self.request.referer if (self.request.referer and self.request.referer not in self.getLoginRoutes()) else self.request.route_url(Conf.postloginroute)
            l.info("postloginpage = {},referer = {}, loginurl = {}, plr = {}, is true? {}".format(
                self.postloginpage, 
                self.request.referer, 
                self.request.route_url("ppsslogin"),
                self.request.route_url(Conf.postloginroute), (self.request.referer and self.request.referer not in self.getLoginRoutes()) )   )
        return self.postloginpage

    def resetPostLoginRoute(self):
        return self.request.session.pop('postloginpage',False)

    def confirmEmailFlow(self, user:PPSsuser, email:str, redirect_url:str=''):
        token = secrets.token_urlsafe()
        hashed_token = getPasswordDigest(token)
        additionaldata = json.dumps({'redirect_url': redirect_url, 'email': email})
        user_token = PPSsResetTokens(token=hashed_token, reset_type='email', additionaldata=additionaldata,  token_expiry=datetime.datetime.now() + timedelta(days = 7))
        user.tokens.append(user_token)
        sendConfirmEmailUseCase(request=self.request).run(user=user, email=email, token=token)

    def changePasswordFlow(self, user:PPSsuser, currentpassword:str, newpassword:str, confirmnewpassword:str, requireoldpassword:bool=True, selfediting:bool=False):
        ##change user password
        if newpassword:

            l.debug("new password match? {}".format(newpassword==confirmnewpassword) )
            #existing user must match older password (unless edited by superadmin)
            if requireoldpassword:
                if (not PPSsuser.checkLogin(user.username,currentpassword,self.request.dbsession)): 
                    if selfediting:
                        raise Exception(__(self.request,"Your current password does not match!"))
                    else:
                        raise Exception(__(self.request,"Current password does not match!"))
                    
            if newpassword==confirmnewpassword:
                l.info("*****changing password for {}".format(user.username) )
                res = checkPassword(user,newpassword)
                l.info("checkPassword result for user {} = {}({})".format(user.username, bool(res),res.getMsg(self.request) ))
                if res:
                    user.setPassword(newpassword)
                    return True
                else:
                    raise Exception(res.getMsg(self.request))
            else:
                raise Exception(__(self.request,"New password doesn't match confirmation field."))


    LOGINROUTES = None
    def getLoginRoutes(self):        
        if AuthController.LOGINROUTES is None:
            AuthController.LOGINROUTES = (
                self.request.route_url("ppsslogin"),
                self.request.route_url("ppss:user:changepassword"),
                self.request.route_url("ppss:user:resetpassword")
            )
        return AuthController.LOGINROUTES

    @view_config(route_name='ppsslogin',renderer=Conf.logintemplate)
    def login(self):
        r = self.request
        postloginpage = self.getPostLoginRoute()
        
        ##TODO: check if correct
        if self.request.referer:
            if self.request.loggeduser:
                signinreason = "Your current account has insufficent rights to view the page you requested.</br>You can change the account you are using with a new login."                
            else:
                signinreason = "Please login to view the requested content."
        else:
            signinreason = ""
        self.retdict["signinreason"] = signinreason
        self.retdict["email_is_required"] = Conf.email_is_required

        self.request.session['postloginpage'] = postloginpage
        if self.user and self.user.passwordExpired():
            return HTTPFound(r.route_url("ppss:user:changepassword"))

        if r.POST:
            username = r.params.get("username",u"")
            password = r.params.get("password",u"")
            if self.isCaptchaEnabled():
                captcha = r.params.get("cf-turnstile-response",u"")
                if not captcha:
                    self.retdict["msg"] = "Captcha is required."
                    return self.retdict
                if self.verifyCaptcha(captcha) == False:
                    self.retdict["msg"] = "Captcha verification failed."
                    return self.retdict
            superuser = False
            res = None
            l.info("Login attempt: u={username}".format(username=username))
            if res is None:
                res,valid = PPSsuser.checkLogin(username,password,r.dbsession,ipaddr = self.request.remote_addr)
                self.logloginattempt(res,valid,username)
            if res and valid:
                res.getPermissionsMap()
                sessionvals = self.getSessionVals(self.setPrincipalsInSession(res,superuser,res.passwordExpired(), res.is2faEnabled()))                
                r.dbsession.expunge(res)
                l.info(f"session keys:{sessionvals}")


                ## log the last login
                llogin = PPSsuser.byId(res.id,r.dbsession)
                llogin.lastlogin = datetime.datetime.now()  
                l.debug("last login:{}".format(llogin.lastlogin))
                r.session.invalidate()
                
                r.session.update(sessionvals)                
                r.session[Conf.sessionuser] = {'id':res.id,'name':username,'user':res}
                headers = remember(r, res.id)
                r.session.save()
                
                return HTTPFound(postloginpage,headers=headers)
            #wrong password or disabled user
            
            l.warn("Login attempt failed for user {user}".format(user=username))
            if valid.blocked():
                msgorig = _('Your account seems to be temporarly blocked. Please wait before retrying or contact your adminstrator.')
            else:
                msgorig = _('something went wrong with your login. Please check your informations')
            msg = r.localizer.translate(msgorig,domain="ppss_auth")
            l.info("message is {} with locale '{}' and domain {}".format(msg,r.localizer.locale_name,msgorig.domain))
            self.retdict.update({'logintpl': Conf.publictemplateinherit ,'msg':msg})
            return self.retdict
        self.retdict.update({'logintpl': Conf.publictemplateinherit , 'msg':''})
        return self.retdict
    
    @view_config(route_name='ppss:user:verifyotp', permission='ppss_auth_otpverify', renderer=Conf.verify2fatpl)
    def verify_otp(self):
        if not self.user.needVerifyOTP(self.request):
            l.info("user {user} does not need 2fa verification".format(user=self.user.username))
            self.request.usersession.otpPass()
            self.logloginattempt(self.user, OtpValidity(loginreason=LOGINREASON.OTPSKIPPED), self.user.username)
            self.request.tm.commit()
            self.request.tm.begin()
            return HTTPFound(self.request.session['postloginpage'])

        if self.request.POST:
            otp = self.request.POST.get('otp', None)
            if otp:
                is_otp_valid = OtpValidity.isOtpValid(otp=otp, otp_hash=self.user.otp_hash)
                if is_otp_valid:
                    postloginpage = self.request.session['postloginpage']
                    
                    self.request.usersession.otpPass()

                    self.logloginattempt(self.user, OtpValidity(loginreason=LOGINREASON.OTPPASSED), self.user.username)
                    l.info("user {user} verified by otp".format(user=self.user.username))
                    self.request.tm.commit()
                    self.request.tm.begin()
                    return HTTPFound(postloginpage)
                else:
                    l.info("user {user} failed to verify otp".format(user=self.user.username))
                    self.logloginattempt(self.user, OtpValidity(loginreason=LOGINREASON.OTPERROR), self.user.username)
                    self.request.tm.commit()
                    self.request.tm.begin()
            self.retdict['msg'] = __(self.request,'Something went wrong... please retry')
            #return self.retdict 
        return self.retdict


    @view_config(route_name='ppsslogout')
    def logout(self):
        l.debug("logout")
        l.debug("principals = {pr}".format(pr=self.request.session.get('principals',[])  ))

        headers = forget(self.request)
        self.request.usersession.clean()
        
        return HTTPFound(self.request.route_url(Conf.postlogoutroute),headers=headers)

    def oauthCallBack(self):
        return Response("OK")

    def registernewuser(self):
        if not Conf.usercanregister:
            HTTPFound(self.request.route_url('ppsslogin'))
        retdict = self.retdict
        retdict["link"] = ["",""]
        retdict["email_is_required"] = Conf.email_is_required
        if self.request.POST:
            cancreate = True
            username = self.request.params.get("username","")
            password = self.request.params.get("password","")
            confirmnewpassword = self.request.params.get("confirmnewpassword","")
            email = self.request.params.get("email",None)
            if Conf.email_is_required:
                if not email:
                    retdict['msg'] += __( self.request,"Email is required")
                    cancreate = False
                if not email_is_valid(email):
                    retdict['msg'] += __( self.request,"Email is invalid")
                    cancreate = False
            if (email != ""):
                if len(PPSsuser.byField("email",email,self.request.dbsession))>0:
                        retdict['msg'] += "Email '{}' already used.".format(email)
                        cancreate = False
            
            if not username:
                retdict["msg"] +=__(self.request,"Username can not be empty.")
                cancreate = False
            if not password:
                retdict["msg"] +=__(self.request,"Password can not be empty.")
                cancreate = False
            if password != confirmnewpassword:
                retdict["msg"] +=__(self.request,"Password check doesn't match the password.")
                cancreate = False
            chkres = checkPassword(PPSsuser(username=username),password)
            if not chkres:
                retdict["msg"] += chkres.getMsg(self.request)
                #__(self.request,"Password doesn't respect minimum constraints.")
                cancreate = False
            if cancreate:
                # user = PPSsuser(username = username, newemail=email, newemailmagic=uuid4(), enabled=-1)
                enabled = -1 if Conf.email_is_required else 1
                user = PPSsuser(username = username, enabled=enabled)
                user.setPassword(password)
                for g in Conf.newusergroups:
                    group = PPSsgroup.byName(g,self.request.dbsession)
                    if group:
                        user.groups.append(group)
                self.request.dbsession.add(user)

                # email verify
                self.confirmEmailFlow(user, email)
                
                if Conf.email_is_required:
                    retdict['msg'] = __(self.request,"Please check your email for activation link.")
                else:
                    retdict['msg'] = __(self.request,"User created, please go to login page.")
                    
                retdict["link"] = ["login",self.request.route_url("ppsslogin")]
            
            # HTTPFound(self.request.route_url("ppsslogin"))
        return retdict
        
    @view_config(route_name="ppss:user:changepassword",renderer=Conf.changepasswordtemplate,permission='ppss_auth_changepassword')
    def ppsschangepassword(self):
        l.debug("change password")
        if not Conf.sessionuser in self.request.session:
            return HTTPFound(self.request.route_url("ppsslogin"))
        message = ""
        forcedtochange=False
        if self.user.passwordExpired():
            #dead code here, message never used
            message = Conf.passwordexpiredmessage
            forcedtochange=True
        
        retdict = {'logintpl': Conf.publictemplateinherit,'msg':message,'res':True}
        retdict.update(self.retdict)
        
        if self.request.POST:
            oldpassword = self.request.params.get("oldpassword")
            newpassword = self.request.params.get("newpassword")
            confirmnewpassword = self.request.params.get("confirmnewpassword","")
            if newpassword!=confirmnewpassword:
                retdict['res']=False
                retdict['msg']=__(self.request,"Password check doesn't match the password.")
                return retdict
            username = self.request.session.get(Conf.sessionuser).get("name")
            user,valid = PPSsuser.checkLogin(username,oldpassword,self.request.dbsession)     

            if valid:
                res = checkPassword(user,newpassword)
                l.info("checkPassword result for user {} = {}".format(user.username, res.getMsg(self.request) ))
                if res:
                    user.setPassword(newpassword)
                    retdict['msg'] = __(self.request,"Password updated.")
                    self.request.loggeduser.passwordexpire = user.passwordexpire
                    l.info("password upadated for user '{}'".format(user.username))
                else:
                    retdict['res']=False
                    retdict['msg'] = res.getMsg(self.request)
                    #__(self.request,"New password doesn't match constraints." )
                    l.info("password upadated failed for user '{}'".format(user.username))
            else:
                retdict['res']=False
                retdict['msg']=__(self.request,'Old password is wrong')
        return retdict
        
    def setPrincipalsInSession(self,user:PPSsuser,isSuperUser=None,passwordExpired=None, is2faEnabled=None):
        if isSuperUser is None:
            isSuperUser = user.isSuperUser()
        if passwordExpired is None:
            passwordExpired  = user.passwordExpired()
        r = self.request
        r.session['principals'],r.session['admin'] = user.getPrincipals()
        #r.session['admin'] = True if isSuperUser else False
     
        l.info("permissions for {}({},{}):{}".format(user.username,isSuperUser,passwordExpired,r.session['principals'])  )
        r.session.changed()
        return r.session

    def getSessionVals(self,session,all=False):
        allvals = {k:session.get(k) for k in session.keys() if (not k.startswith("_")) or all }
        l.debug(f"all session vals:{allvals}")
        return allvals
    
    def enable_2fa(self):
        self.retdict['errmsg'] = None
        otp_hash = OtpValidity.get_hash()
        self.retdict['img_str'] = OtpValidity.qrcode_img_from_otp_hash(username=self.user.username,otp_hash=otp_hash)
        self.retdict['otp_hash'] = otp_hash

        if self.request.POST:
            otp = self.request.POST.get('otp',None)
            otp_hash = self.request.POST.get('otp_hash',None)
            
            if otp and otp_hash:
                is_otp_valid = OtpValidity.isOtpValid(otp=otp, otp_hash=otp_hash)
                if is_otp_valid:
                    self.user.otp_hash = otp_hash
                    self.request.tm.commit()
                    self.request.tm.begin()
                    self.request.loggeduser.otp_hash = otp_hash
                    self.request.usersession.otpPass()
                    msg = {"msg": __(self.request,'Successfully setup 2fa'), "type": "success"}
                    self.request.session.flash(msg)
                    
                    return HTTPFound(location=self.getPostLoginRoute())
                else:
                    self.retdict['errmsg'] = __(self.request,'Otp incorrect or expired ... please retry')
                    return self.retdict
            else:
                self.retdict['errmsg'] = __(self.request,'Something went wrong... please retry')
                return self.retdict
        return self.retdict




    @view_config(route_name='test:test',permission='listuser',renderer=Conf.logintemplate)
    def testroute(self):
        return {}

    @view_config(route_name='ppss:user:email:confirm', renderer=Conf.confirm_email_template)
    def confirm_email(self):
        token = self.request.params.get('token', None)
        self.retdict['login_url'] = ""
        self.retdict['msg_type'] = "danger"
        self.retdict['redirect_url'] = False
        if not token:
            self.retdict['msg'] = __(self.request,"Invalid token")
            return self.retdict
        
        hashed_token = getPasswordDigest(token)
        db_token = PPSsResetTokens.firstByField('token', hashed_token, self.request.dbsession)

        if not db_token:
            self.retdict['msg']=__(self.request,"Invalid token")
            return self.retdict
        
        user:PPSsuser = db_token.user
        # lock all tokens of the user
        user_tokens = self.request.dbsession.query(PPSsResetTokens).filter(PPSsResetTokens.user_id == user.id).filter(PPSsResetTokens.reset_type == 'email').with_for_update().all()
            
        matched_token:PPSsResetTokens = None
        for user_token in user_tokens:
            if user_token.token == hashed_token:
                matched_token = user_token
        if not matched_token:
            self.retdict['msg']=__(self.request,"Invalid token")
            return self.retdict
        
        if matched_token.token_expiry < datetime.datetime.now():
            self.retdict['msg']=__(self.request,"Token has expired. Please try again")
            return self.retdict
        additionaldata = json.loads(matched_token.additionaldata)
        user.email = additionaldata['email']
        if user.enabled == -1:
            user.enabled = 1
        self.retdict['msg'] = __(self.request,"Email confirmed")
        self.retdict['login_url'] = self.request.route_url('ppsslogin')
        self.retdict['redirect_url'] = additionaldata.get('redirect_url', '')
        self.retdict['msg_type'] = "success"

        for user_token in user_tokens:
            self.request.dbsession.delete(user_token)
        return self.retdict


    @view_config(route_name='ppss:user:recoverpassword', renderer=Conf.recoverpasstemplate)
    def recover(self):
        if self.request.POST:
            email = self.request.params.get("email",None)
            if not email:
                self.retdict['msg'] = __( self.request,"Email is required")
                return self.retdict
            if not email_is_valid(email):
                self.retdict['msg'] = __( self.request,"Email is invalid")
                return self.retdict
            user:PPSsuser = PPSsuser.firstByField('email', email, self.request.dbsession)
            l.info("user is %s", user)
            # same response so that we don’t give attackers any indication that they should try a different email address.
            self.retdict['msg'] = __(self.request, "Please check your email for reset link")
            if user:
                token = secrets.token_urlsafe()
                hashed_token = getPasswordDigest(token)
                user_token = PPSsResetTokens(token=hashed_token, reset_type='password')
                user.tokens.append(user_token)
                sendPasswordResetEmailUseCase(self.request).run(user=user, token=token)
        return self.retdict

    @view_config(route_name='ppss:user:resetpassword', renderer=Conf.resetpasstpl)
    def reset_password(self):
        
        token = self.request.params.get('token', None)
        self.retdict['token'] = token
        self.retdict['login_url'] = ""
        self.retdict['password_requirements'] = Conf.passwordwrongmessage
            
        if self.request.POST:
            token = self.request.params.get('reset-token', None)
            if not token:
                self.retdict['msg']=__(self.request,"Invalid token")
                return self.retdict
            hashed_token = getPasswordDigest(token)
            db_token = PPSsResetTokens.firstByField('token', hashed_token, self.request.dbsession)
            if not db_token:
                self.retdict['msg']=__(self.request,"Invalid token")
                return self.retdict
            user:PPSsuser = db_token.user

            # lock all tokens of the user
            user_tokens = self.request.dbsession.query(PPSsResetTokens).filter(PPSsResetTokens.user_id == user.id).filter(PPSsResetTokens.reset_type == 'password').with_for_update().all()
            
            matched_token:PPSsResetTokens = None
            for user_token in user_tokens:
                if user_token.token == hashed_token:
                    matched_token = user_token
            if not matched_token:
                self.retdict['msg']=__(self.request,"Invalid token")
                return self.retdict
            
            if matched_token.token_expiry < datetime.datetime.now():
                self.retdict['msg']=__(self.request,"Token has expired. Please try again")
                return self.retdict
            
            # validate input
            newpassword = self.request.params.get("newpassword")
            confirmnewpassword = self.request.params.get("confirmnewpassword","")
            if newpassword!=confirmnewpassword:
                self.retdict['msg']=__(self.request,"Password check doesn't match the password.")
                return self.retdict
            # validate magic number
            
            res = checkPassword(user,newpassword)
            if res:
                user.setPassword(newpassword)
                self.retdict['msg'] = __(self.request,"Password updated.")
                self.retdict['login_url'] = self.request.route_url('ppsslogin')
                for user_token in user_tokens:
                    self.request.dbsession.delete(user_token)
            else:
                self.retdict['msg'] = res.getMsg(self.request)
            
            return self.retdict
            
            
        return self.retdict


import re
EMAIL_REGEX = re.compile(r"[^@]+@[^@]+\.[^@]+")
def email_is_valid(email):
    if EMAIL_REGEX.match(email):
        return True
    return False
