import logging
import re

import pyotp

from ppss_auth.ppss_auth_utils.i18n import __
from ppss_auth.ppss_auth_utils.otp import OtpValidity
from ppss_auth.ppss_auth_utils.password import checkPassword
from ..constants import Conf
from ppss_auth.models import PPSspermission, PPSsuser, PPSsgroup
from ppss_auth.views.auth import AuthController, email_is_valid
from pyramid.view import view_defaults, view_config
from pyramid.httpexceptions import HTTPBadRequest, HTTPFound, HTTPNotFound
from pyramid.renderers import render_to_response
from sqlalchemy.exc import IntegrityError
from pyramid.response import Response
import json

l = logging.getLogger("ppssauth")


@view_defaults(require_csrf=True)
class CrudController(AuthController):

    def getInitialParams(self):
        userid = self.userid
        user = self.user
        selfediting = False
        if self.request.matched_route.name == "ppss:user:editself":
            userid = self.request.session[Conf.sessionuser]["id"]
            user = self.getUserById(userid)
            selfediting = True
        else:
            if userid == self.request.loggeduser.id:
                selfediting = True

        requireoldpassword = userid >= 0 and (
            selfediting or self.request.loggeduser.isSuperUser() == False
        )

        l.debug(f"{userid=} {user=} {selfediting=} {requireoldpassword=}")

        return userid, user, selfediting, requireoldpassword

    def getEditableGroups(self, selfediting):
        editablegroups = []
        logged_user = self.request.loggeduser
        if logged_user.isSuperUser():
            editablegroups = PPSsgroup.all(self.request.dbsession)
        elif logged_user.hasPermission("edituser") or selfediting:
            editablegroups = logged_user.groups
        return editablegroups

    @view_config(
        route_name="ppss:user:list",
        permission="listuser",
        renderer=Conf.listusertemplate,
    )
    def listUser(self):
        elements = self.request.dbsession.query(PPSsuser).all()
        retdict = {"elements": elements}
        retdict.update(self.retdict)
        if self.request.POST:

            username = self.request.params.get("username", None)
            if username:
                try:
                    newuser = PPSsuser(username=username, enabled=0)
                    self.request.dbsession.add(newuser)
                    self.request.dbsession.flush()
                    return HTTPFound(
                        location=self.request.route_url(
                            "ppss:user:edit", elementid=newuser.id
                        )
                    )
                except IntegrityError as e:
                    self.request.dbsession.rollback()
                    self.request.session.flash(
                        {"msg": "Username already exist", "type": "danger"}
                    )
                except Exception as generic:
                    self.request.dbsession.rollback()
                    self.request.session.flash(
                        {"msg": "Something went wrong, retry later", "type": "danger"}
                    )
                    l.exception(f"exception in add new user: {generic}")
                finally:
                    return HTTPFound(location=self.request.current_route_url())
            else:
                self.request.session.flash(
                    {"msg": "Username is required", "type": "danger"}
                )
                return HTTPFound(location=self.request.current_route_url())
        return retdict

    @view_config(
        route_name="ppss:user:email:edit",
        permission="login",
        request_method="POST",
        renderer="json",
    )
    def editUserEmail(self):
        userid, user, selfediting, requireoldpassword = self.getInitialParams()
        if not selfediting and not user.hasPermission("edituser"):
            return HTTPBadRequest(
                text=json.dumps({"res": False, "msg": "Can't edit email for other users"}),
                content_type="application/json",
            )
            
        email = self.request.params.get("email", None)
        if not email:
            return HTTPBadRequest(
                text=json.dumps({"res": False, "msg": "Missing email"}),
                content_type="application/json",
            )

        if isinstance(user, PPSsuser):
            self.confirmEmailFlow(user, email, redirect_url=self.request.route_url("ppss:user:editself"))
            return {
                "res": True,
                "msg": "Check your email for confirmation link, after confirmation refresh this page",
            }
        else:
            return HTTPNotFound(
                text=json.dumps({"res": False, "msg": "User not found"}),
                content_type="application/json",
            )

    @view_config(
        route_name="ppss:user:password:edit",
        permission="login",
        request_method="POST",
        renderer="json",
    )
    def editUserPassword(self):
        userid, user, selfediting, requireoldpassword = self.getInitialParams()
        if not selfediting and not user.hasPermission("edituser"):
            return HTTPBadRequest(
                text=json.dumps({"res": False, "msg": "Can't edit password for other users"}),
                content_type="application/json",
            )
        currentpassword = self.request.params.get("currentpassword", None)
        newpassword = self.request.params.get("password", None)
        confirmnewpassword = self.request.params.get("confirmnewpassword", None)

        if requireoldpassword and not currentpassword:
            return HTTPBadRequest(
                text=json.dumps({"res": False, "msg": "Missing current password"}),
                content_type="application/json",
            )

        if not newpassword or not confirmnewpassword:
            return HTTPBadRequest(
                text=json.dumps(
                    {"res": False, "msg": "Missing password or confirm new password"}
                ),
                content_type="application/json",
            )

        if isinstance(user, PPSsuser):
            try:
                self.changePasswordFlow(
                    user,
                    currentpassword,
                    newpassword,
                    confirmnewpassword,
                    requireoldpassword,
                    selfediting,
                )
                return {"res": True, "msg": "Password updated"}
            except Exception as e:
                return HTTPBadRequest(
                    text=json.dumps({"res": False, "msg": str(e)}),
                    content_type="application/json",
                )
        else:
            return HTTPNotFound(
                text=json.dumps({"res": False, "msg": "User not found"}),
                content_type="application/json",
            )

    @view_config(
        route_name="ppss:user:enable:edit",
        permission="edituser",
        request_method="POST",
        renderer="json",
    )
    def editUserEnable(self):
        userid, user, selfediting, requireoldpassword = self.getInitialParams()
        if selfediting:
            return HTTPBadRequest(
                text=json.dumps({"res": False, "msg": "Can't enable/disable yourself"}),
                content_type="application/json",
            )
        enabled = 1 if self.request.params.get("enabled") == "1" else 0
        if isinstance(user, PPSsuser):
            if user.password:
                user.enabled = enabled
                return {
                    "res": True,
                    "msg": f"User {'enabled' if enabled else 'disabled'}",
                }
            else:
                return HTTPBadRequest(
                    text=json.dumps(
                        {"res": False, "msg": "User has no password, can't enable"}
                    ),
                    content_type="application/json",
                )
        else:
            return HTTPNotFound(
                text=json.dumps({"res": False, "msg": "User not found"}),
                content_type="application/json",
            )

    @view_config(
        route_name="ppss:user:groups:edit",
        permission="edituser",
        request_method="POST",
        renderer="json",
    )
    def editUserGroups(self):
        userid, user, selfediting, requireoldpassword = self.getInitialParams()
        editablegroups = self.getEditableGroups(selfediting)
        groups = map(int, self.request.params.getall("allgroups"))
        usergroups = [
            PPSsgroup.byId(groupid, self.request.dbsession)
            for groupid in groups
            if groupid in set([g.id for g in editablegroups])
        ]
        user.groups = usergroups
        return {"res": True, "msg": f"User groups updated"}

    @view_config(
        route_name="ppss:user:edit",
        permission="edituser",
        renderer=Conf.editusertemplate,
    )
    @view_config(
        route_name="ppss:user:editself",
        permission="login",
        renderer=Conf.editusertemplate,
    )
    def editUser(self):
        l.info("edit user")
        userid, user, selfediting, requireoldpassword = self.getInitialParams()

        l.debug("***{id} -> {user}".format(user=user, id=userid))
        retdict = dict(
            self.retdict,
            **{
                "msg": "",
                "res": True,
                "userid": userid,
                "submiturl": "",
                "selfediting": selfediting,
                "email_is_required": Conf.email_is_required,
            },
        )

        if not user:
            retdict["res"] = False
            retdict["msg"] = __(self.request, "User not found")

        editablegroups = self.getEditableGroups(selfediting)

        # requireoldpassowrd = userid >= 0 and ( selfediting or logged_user.isSuperUser()==False )

        retdict.update(
            {
                "user": user,
                "allgroups": editablegroups,
                "requireoldpassword": requireoldpassword,
            }
        )
        
        return retdict

        # if self.request.POST:
        #     canenable = True

        #     email = self.request.params.get("email",None)
        #     if Conf.email_is_required:
        #         if not email:
        #             retdict['msg'] = __( self.request,"Email is required")
        #             return retdict
        #         if not email_is_valid(email):
        #             retdict['msg'] = __( self.request,"Email is invalid")
        #             return retdict
        #     if (email != "") and (email != user.email):
        #         if len(PPSsuser.byField("email",email,self.request.dbsession))>0:
        #             retdict['msg'] = "Email '{}' already used.".format(email)
        #             return retdict

        #     if userid<0:
        #         l.debug("this is a post for creation")
        #         username = self.request.params.get("username",None)
        #         if not username:
        #             retdict['msg'] = __("Username can not be empty.")
        #             return retdict
        #         if len(PPSsuser.byField("username",username,self.request.dbsession))>0:
        #             retdict['msg'] = "Username '{}' already used.".format(username)
        #             return retdict
        #         self.request.dbsession.add(user)
        #         user.username = username
        #         # default for new user is "Cant enable". Can be enabled ony if a valid password is supplied
        #         canenable = False
        #     username = user.username

        #     if not user:
        #         return retdict

        #     newpassword = self.request.params.get("password","")
        #     ##change user password
        #     if newpassword:
        #         confirmnewpassword = self.request.params.get("confirmnewpassword","")
        #         currentpassword = self.request.params.get("currentpassword","")
        #         l.debug("new password match? {}".format(newpassword==confirmnewpassword) )
        #         # existing user must match older password (unless edited by superadmin)
        #         # if (self.request.loggeduser.isSuperUser() == False) or selfediting:
        #         if requireoldpassowrd:
        #             if userid>=0 and (not PPSsuser.checkLogin(username,currentpassword,self.request.dbsession)):
        #                 if selfediting:
        #                     retdict['msg'] = __(self.request,"Your current password does not match!")
        #                 else:
        #                     retdict['msg'] = __(self.request,"Current password does not match!")
        #                 return retdict

        #         if newpassword==confirmnewpassword:
        #             l.info("*****changing password for {}".format(user.username) )
        #             res = checkPassword(user,newpassword)
        #             l.info("checkPassword result for user {} = {}({})".format(user.username, bool(res),res.getMsg(self.request) ))
        #             if res:
        #                 user.setPassword(newpassword)
        #                 retdict['msg'] = __(self.request,"Password updated.")
        #                 canenable = True
        #             else:
        #                 retdict['msg'] = res.getMsg(self.request)
        #                 # __(self.request,"New password doesn't match constraints." )
        #         else:
        #             retdict['msg'] = __(self.request,"New password doesn't match confirmation field.")
        #     user.enabled = 1 if self.request.params.get("enabled")=="1" and canenable and user.password else 0

        #     groups=map(int,self.request.params.getall("allgroups"))
        #     l.debug("group={groups}".format(groups=groups ))
        #     usergroups = [PPSsgroup.byId(groupid,self.request.dbsession) for groupid in groups if groupid in set([g.id for g in editablegroups ])]
        #     user.groups = usergroups
        #     self.request.dbsession.flush()
        # return HTTPFound(self.request.route_url('ppss:user:edit',elementid = user.id) )
        # return retdict
        # return retdict

    def listGroup(self):
        elements = self.request.dbsession.query(PPSsgroup).all()
        return dict(self.retdict, **{"elements": elements})

    def editGroup(self):
        groupid = int(self.request.matchdict.get("elementid", "-1"))
        retdict = dict(self.retdict, **{"msg": "", "res": True, "groupid": groupid})
        if groupid < 0:
            group = PPSsgroup()
        else:
            group = PPSsgroup.byId(groupid, self.request.dbsession)
            if not group:
                return HTTPFound(self.request.route_url("ppss:group:list"))
        retdict.update({"group": group})

        if self.request.POST:  # editing group
            if groupid < 0:
                self.request.dbsession.add(group)
            group.name = self.request.params.get("name")
            group.enabled = 1 if self.request.params.get("enablecheck") == "1" else 0
            l.debug("paratri: {p}".format(p=self.request.params))
            l.debug(
                "group.name={name},  group.enabled={enabled}".format(
                    name=group.name, enabled=group.enabled
                )
            )
            elements = self.request.dbsession.query(PPSsgroup).all()
            return dict(retdict, **{"elements": elements})

        elif group:
            allperm = self.request.dbsession.query(PPSspermission).all()
            users = self.request.dbsession.query(PPSsuser).all()
            return render_to_response(
                Conf.editgrouptemplate,
                dict(
                    retdict,
                    **{"group": group, "allperm": allperm, "users": users, "msg": ""},
                ),
                self.request,
            )
        # return HTTPFound(self.request.route_url("ppss:group:list") )

    def listPerm(self):
        elements = self.request.dbsession.query(PPSspermission).all()
        return dict(self.retdict, **{"elements": elements})

    def editPerm(self):
        pid = int(self.request.matchdict.get("elementid", -1))
        if pid < 0:
            perm = PPSspermission(id=pid)
        else:
            perm = PPSspermission.byId(pid, self.request.dbsession)

        if self.request.POST:
            elements = self.request.dbsession.query(PPSspermission).all()
            name = self.request.params.get("name", "")
            if pid < 0:
                self.request.dbsession.add(PPSspermission(name=name))
            elif perm.permtype != 1:
                perm.name = name
            else:
                res = {
                    "res": False,
                    "msg": __(self.request, "Can't modify this permission"),
                }
                return dict(self.retdict, **dict(res, **{"elements": elements}))
            res = {"res": True, "msg": __(self.request, "Permission modified")}
            return dict(self.retdict, **dict(res, **{"elements": elements}))
        elif perm:
            return render_to_response(
                Conf.editpermtemplate,
                dict(self.retdict, **{"perm": perm}),
                self.request,
            )
        return HTTPFound(self.request.route_url("ppss:perm:list"))

    def deletePerm(self):
        perm = PPSspermission.byId(int(self.request.matchdict.get("elementid", -1)))

        if perm and perm.permtype != 1:
            self.request.dbsession.delete(perm)
            res = {"res": True, "msg": __(self.request, "Permission deleted.")}
        else:
            res = {
                "res": False,
                "msg": __(self.request, "Can't delete this permission"),
            }
        elements = self.request.dbsession.query(PPSspermission).all()
        return dict(self.retdict, **dict(res, **{"elements": elements}))

    @view_config(
        route_name="ppss:user:editotp", permission="login", renderer=Conf.edit2fatpl
    )
    def view2fa(self):
        self.retdict["errmsg"] = None
        self.retdict["img_str"] = OtpValidity.qrcode_img_from_otp_hash(
            self.user.username, self.user.otp_hash
        )
        self.retdict["current_otp_hash"] = self.user.otp_hash
        self.retdict["otp_hash"] = OtpValidity.get_hash()
        self.retdict["img_str_new"] = OtpValidity.qrcode_img_from_otp_hash(
            self.user.username, self.retdict["otp_hash"]
        )
        if self.request.POST:
            otp = self.request.POST.get("otp", None)
            otp_hash = self.request.POST.get("otp_hash", None)
            if otp and otp_hash:
                is_otp_valid = OtpValidity.isOtpValid(otp=otp, otp_hash=otp_hash)
                if is_otp_valid:
                    self.user.otp_hash = otp_hash
                    msg = {
                        "msg": __(self.request, "Successfully changed 2fa"),
                        "type": "success",
                    }
                    self.request.session.flash(msg)
                    return HTTPFound(self.request.route_url("ppss:user:editself"))
            self.retdict["errmsg"] = __(
                self.request, "Something went wrong... please retry"
            )
        return self.retdict

    def addPerm2Group(self):
        perm = PPSspermission.byId(
            int(self.request.matchdict.get("targetid", -1)), self.request.dbsession
        )
        group = PPSsgroup.byId(
            int(self.request.matchdict.get("elementid", -1)), self.request.dbsession
        )
        if not perm or not group:
            return {"res": False, "msg": __(self.request, "error in ids")}
        for i in group.permissions:
            if i.id == perm.id:
                return {"res": False, "msg": __(self.request, "already present")}
        group.permissions.append(perm)
        l.info("adding {perm} to {group}".format(perm=perm, group=group))
        return {"res": True, "msg": "change_perm", "groupperm": group.permdict()}

    def removePerm2Group(self):
        perm = PPSspermission.byId(
            int(self.request.matchdict.get("targetid", -1)), self.request.dbsession
        )
        group = PPSsgroup.byId(
            int(self.request.matchdict.get("elementid", -1)), self.request.dbsession
        )

        if perm and (perm.permtype != "y"):  # TODO add superadmin capability to do this
            for i, p in enumerate(group.permissions):
                l.info("check {} {}".format(p.id, perm.id))
                if p.id == perm.id:
                    l.info("match")
                    group.permissions.pop(i)
                    return {
                        "res": True,
                        "msg": "change_perm",
                        "groupperm": group.permdict(),
                    }
                else:
                    l.info("no match")

        return {"res": False, "msg": __(self.request, "Can't remove this permission")}

    def addUser2Group(self):
        user = PPSsuser.byId(
            int(self.request.matchdict.get("targetid", -1)), self.request.dbsession
        )
        group = PPSsgroup.byId(
            int(self.request.matchdict.get("elementid", -1)), self.request.dbsession
        )
        if not user or not group:
            return {"res": False, "msg": "error in ids"}
        for i in group.users:
            if i.id == user.id:
                return {"res": False, "msg": __(self.request, "already present")}
        group.users.append(user)
        l.info("adding {user} to {group}".format(user=user, group=group))
        return {"res": True, "msg": "change_user", "elements": group.userdict()}

    def removUser2Group(self):
        user = PPSsuser.byId(
            int(self.request.matchdict.get("targetid", -1)), self.request.dbsession
        )
        group = PPSsgroup.byId(
            int(self.request.matchdict.get("elementid", -1)), self.request.dbsession
        )
        if user:  # TODO add superadmin capability to do this
            for i, p in enumerate(group.users):
                if p.id == user.id:
                    group.users.pop(i)
                    return {
                        "res": True,
                        "msg": __(self.request, "change_user"),
                        "elements": group.userdict(),
                    }
        return {"res": False, "msg": __(self.request, "Can't remove this permission")}

    def parseqstring(self, qparam):
        if qparam == "" or qparam is None:
            return ""
        qparam = " " + qparam + " "
        qparam = re.sub("[%]+", "\\%", qparam)
        qparam = re.sub("[ ]+", "%", qparam)
        return qparam

    @view_config(route_name="ppss:user:search", permission="listuser", renderer="json")
    def searchUser(self):
        qparam = self.parseqstring(self.request.params.get("q", ""))
        l.debug("qparam = {qp}".format(qp=qparam))
        users = (
            self.request.dbsession.query(PPSsuser)
            .filter(PPSsuser.enabled == 1)
            .filter(PPSsuser.username.like(qparam))
            .all()
        )
        return {"res": True, "elements": [u.todict() for u in users]}

    @view_config(route_name="ppss:group:search", permission="listuser", renderer="json")
    def searchGroup(self):
        qparam = self.parseqstring(self.request.params.get("q", ""))
        users = (
            self.request.dbsession.query(PPSsgroup)
            .filter(PPSsgroup.enabled == 1)
            .filter(PPSsgroup.name.like(qparam))
            .all()
        )
        return {"res": True, "elements": [u.todict() for u in users]}

    @view_config(route_name="ppss:perm:search", permission="listuser", renderer="json")
    def searchParam(self):
        qparam = self.parseqstring(self.request.params.get("q", ""))
        users = (
            self.request.dbsession.query(PPSspermission)
            .filter(PPSspermission.name.like(qparam))
            .all()
        )
        return {"res": True, "elements": [u.todict() for u in users]}
