import base64
from io import BytesIO
import logging

import pyotp
import qrcode

from ppss_auth.constants import Conf
from ppss_auth.models import LOGINREASON

l = logging.getLogger("ppssauth.otp")


class OtpValidity:
    def __init__(self, loginreason) -> None:
        self.loginreason = loginreason

    def result(self):
        return self.loginreason[0]

    def resultreason(self):
        return self.loginreason[1]

    @staticmethod
    def isOtpValid(otp: str, otp_hash: str) -> bool:
        otp = otp.strip()
        totp = pyotp.TOTP(otp_hash)
        l.debug(f"OTP now: {totp.now()}, OTP from user: {otp} ")
        is_otp_valid: bool = totp.verify(otp)
        l.debug(f"is otp valid: {is_otp_valid}")
        return is_otp_valid

    @staticmethod
    def qrcode_img_from_otp_hash(username: str, otp_hash) -> str:
        uri = pyotp.totp.TOTP(otp_hash).provisioning_uri(
            username, issuer_name=Conf.issuer2fa
        )
        image = qrcode.make(uri)
        buffered = BytesIO()
        image.save(buffered, format="JPEG")
        img_str = base64.b64encode(buffered.getvalue())
        img_base64 = bytes("data:image/jpeg;base64,", encoding="utf-8") + img_str
        img_base64_str = img_base64.decode("utf-8")
        return img_base64_str

    @staticmethod
    def get_hash() -> str:
        return pyotp.random_base32()
