from ..constants import Conf
from ..models import(
    PPSspermission,PPSsgroup,PPSsuser
)
import logging
l = logging.getLogger('ppssauth.createdb')


class constants():
    SYSADMINPERM = "sysadmin"
    SYSADMINGROUP = "sysadmin"

def __createPermissions(session,permissions):
    permmap = {}
    for p in permissions:
        perm = session.query(PPSspermission).filter(PPSspermission.name == p['name']).first()
        if not perm:
            perm = PPSspermission(name = p['name'])
            session.add(perm)
            l.info("permission '{}' added to session".format(p['name']))
        perm.permtype = p['permtype'] if 'permtype' in p else 0
        permmap[p['name'] ] = perm
    return permmap

def __createGroups(session,groups,permissions):
    groupmap = {}
    for g in groups:
        g = g.split("=")
        gname = str.strip(g[0])
        if len(g) == 2:
            permlist = map(str.strip,g[1].split(","))
        else:
            permlist = []
        group = session.query(PPSsgroup).filter(PPSsgroup.name == gname).first()
        if not group:
            group = PPSsgroup(name = gname)
            session.add(group)
            l.info("group '{}' added to session".format(gname))
        groupmap[gname] = group
        for p in permlist:
            if not group.hasPermission(p):
                if p in permissions:
                    perm = permissions[p]
                else:
                    perm = PPSspermission(name = p,permtype = 0)
                    session.add(perm)
                    permissions[p] = perm
                    l.info("new permission '{}' created for group '{}'".format(p,gname))
                group.permissions.append(perm)
    return groupmap

def __createUsers(session,users,groups,defaultpassword = None):
    usermap = {}
    for u in users:
        u = u.split("=")
        uname = u[0].split("/")
        upassword = None
        if len(uname)==2:
            upassword = uname[1].strip()
        else: 
            upassword = Conf.defaultpassword
        uname = uname[0].strip()
        if len(u)==2:
            grouplist = map(str.strip,u[1].split(","))
        else:
            grouplist = []
        user = session.query(PPSsuser).filter(PPSsuser.username == uname).first()
        if not user:
            user = PPSsuser(username = uname)
            if upassword:
                user.setPassword(upassword)
            elif defaultpassword:
                user.setPassword(defaultpassword)
            session.add(user)
            l.info("user '{}' added to session".format(uname))
        usermap[uname] = user
        for g in grouplist:
            if not user.isInGroup(g,False):
                if g in groups:
                    group = groups[g]
                else:
                    group = session.query(PPSsgroup).filter(PPSsgroup.name == g).first()
                    if group is None:
                        group = PPSsgroup(name = g)
                        session.add(group)
                    else:
                        groups[g] = group
                    l.info("new group '{}' created for user '{}'".format(g,uname))
                user.groups.append(group)
    return usermap


##the caller must commit this
def initdb(session=None,createdefault=False,engine = None):
    if engine is None:
        engine = session.get_bind()
    try:
        PPSsuser.all(session)
    except:
        raise Exception("no user table. exiting!")
    if createdefault:
        l.info("creating default")
        systemperms = [#PPSspermission(name = u"admin"),
                {"name": u"edituser","permtype": 1},
                {"name": u"listuser","permtype": 1},
                {"name": u"login"   ,"permtype": 1},
                {"name": constants.SYSADMINPERM,"permtype": 1},
        ]
        permissions = [{'name':p,'permtype':1} for p in Conf.perm2create]
        for sp in systemperms:
            permissions.append(sp)

        permissionmap = __createPermissions(session,permissions)
        l.debug("permission map:{}".format(permissionmap))
        groupmap = __createGroups(session, Conf.group2create + 
            [
                "useradmin=edituser,listuser,login",
                "signeduser=login",
                "{}={}".format(constants.SYSADMINGROUP,constants.SYSADMINPERM)
            ] 
            , permissionmap)
        l.debug("group map:{}".format(groupmap))
        if Conf.adminname:
            root = __createUsers(session,[Conf.adminname+"="+constants.SYSADMINGROUP],groupmap,Conf.adminpass if Conf.adminpass else None)
        usermap = __createUsers(session, Conf.user2create, groupmap )
        l.debug("user map:{}".format(usermap))
        return 0
    
    return 0