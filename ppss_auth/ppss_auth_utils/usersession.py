from ..constants import Conf
import logging
l = logging.getLogger('ppssauth.usersession')

class UserSession():
    def __init__(self,user, request) -> None:
        self.request = request
        self.user = user
        self.otppending = request.session.get('otppending',True)

    def clean(self):
        self.request.session.pop('otppending',None)
        self.request.session.pop('admin',None)
        self.request.session.pop(Conf.sessionuser,None)
        self.request.session.pop('principals',None)
        self.request.session.save()
        
    __principals = None
    def getPrincipals(self):
        if self.__principals is None:
            self.__principals,_ = self.__getPrincipals()
        return self.__principals
    
    def passwordExpired(self) -> bool:
        return 'ppss_auth:changepassword' in self.getPrincipals()
    
    def needOTPcheck(self) -> bool:
        return 'ppss_auth:otpverify' in self.getPrincipals()
    
    def needOTPSetup(self) -> bool:
        if Conf.is2faConfigured() and not self.user.is2faEnabled():
            return True
        return False
    
    def otpPass(self):
        self.request.session['otppending'] = False
        self.request.session.save()


    def __getPrincipals(self):
        if self.user is None:
            return []
        
        expired = self.user.passwordExpired() 
        is2faEnabled = self.user.is2faEnabled()

        if is2faEnabled and self.otppending:
            return ['ppss_auth:otpverify'],False
        
        if self.needOTPSetup():
            return ['ppss_auth:otpset'],False
        
        if expired:
            l.info("+++++ user {} has expired password".format(self.user.username))
            return ['ppss_auth:changepassword'],False
        
        groups,_ = self.user.getPrincipals()
        return groups,self.user.isSuperUser()
        
