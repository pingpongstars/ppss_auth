PROJECT=ppss_auth

echo "$PROJECT"

#exit 0

pybabel extract -F babel.ini -o $PROJECT/locale/$PROJECT.pot $PROJECT 
pybabel extract -F babel.ini -k _t:2 -o $PROJECT/locale/$PROJECT.pot $PROJECT 

for LANG in en, it
do
  msgmerge --update $PROJECT/locale/$LANG/LC_MESSAGES/$PROJECT.po $PROJECT/locale/$PROJECT.pot
done