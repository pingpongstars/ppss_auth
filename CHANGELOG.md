# Change log for ppss_auth


##future planned features
- login with social
- forceable email as user id

##1.0 (future)
- pyramid 2 compatibility

##0.10.0
- added email to ppss_user

##0.9.4
- added counter on failed login and threshold against brute force attacks

##0.9.2
- CSFR token added to all post forms

##0.9.0
- some i18n support
- bug fixes on change password

##0.8.3
- user registration

##0.8.2
- unique user
- super user can change others password without older password
- change of hierarchy in jinja2 blocks
- change of block names in templates

##0.8.0
- added migrations to db schema.
- added scripts to upgrade ppss_auth scheme imported by a project
- added check to db schema


##0.7.5
- create super in database
- fix user enabled check

## 0.7.1

- added changelog to project
- fixed issue with required python version

## 0.7
 
- added support to python 3

